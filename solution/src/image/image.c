#include "../../include/image/image.h"
#include <inttypes.h>
#include <stdlib.h>



struct image* create(uint64_t width, uint64_t height) {
    struct image *img = malloc(sizeof(struct image));
    img->width = width;
    img->height = height;
    img->data = malloc(sizeof(struct pixel) * width * height * 3);
    return img;
}

void deinitialize(struct image* img){
    free(img->data);
    free(img);
}
