#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

#include "../../include/formats/bmp.h"
#include "../../include/image/image.h"

enum read_status from_bmp( FILE* in, struct image** img ){
  bmpHeader header;
  struct pixel pixel;
  uint8_t offset;
  fread(&header, sizeof(bmpHeader), 1, in);

  if(header.biBitCount != 24){
    return READ_INVALID_BITS;
  }

  *img = create(header.biWidth, header.biHeight);
  offset = (3*header.biWidth) % 4;
   if (offset != 0) {
     offset = 4 - offset;
   }

  for (size_t i = 0; i < header.biHeight; i++){
    for (size_t j = 0; j < header.biWidth; j++) {
      fread(&pixel, sizeof(struct pixel), 1, in);
     // printf("%d(%zu; %zu) - r:%d, g:%d, b:%d\n", (uint32_t)((*img)->width * i + j), j, i,pixel.r, pixel.g, pixel.b);
      (*img)->data[header.biWidth * i + j] = pixel;
    }

    if (offset != 0){
      fread(&pixel, sizeof(unsigned char)*offset, 1, in);
    }
  }

  return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ){

  size_t padding = (4 - (img->width * sizeof(struct pixel) % 4)) % 4;
  bmpHeader bmpHeader = {0};
  struct pixel pixel;

  bmpHeader.bfType = 0x4D42;
  bmpHeader.bfReserved = 0;
  bmpHeader.bOffBits = 54;
  bmpHeader.biSize = 40;
  bmpHeader.biWidth = img->width;
  bmpHeader.biHeight = img->height;
  bmpHeader.biPlanes = 1;
  bmpHeader.biBitCount = 24;
  bmpHeader.biCompression = 0;
  bmpHeader.biSizeImage = (img->width * sizeof(struct pixel) + padding) * img->height;
  bmpHeader.bfileSize = bmpHeader.biSizeImage + sizeof(bmpHeader);
  bmpHeader.biXPelsPerMeter = 0;
  bmpHeader.biYPelsPerMeter = 0;
  bmpHeader.biClrUsed = 0;
  bmpHeader.biClrImportant = 0;

  uint8_t offset = (4 - (bmpHeader.biWidth * sizeof(struct pixel) % 4)) % 4;

  fwrite(&bmpHeader, sizeof(bmpHeader), 1, out);
  fseek(out, bmpHeader.bOffBits, SEEK_SET);

  for (size_t i = 0; i < img->height; i++) {
    for (size_t j = 0; j < img->width; j++) {
      pixel = img->data[img->width * i + j];
      // printf("%lu - r:%d, g:%d, b:%d\n", img->width * i + j, pixel.r, pixel.g, pixel.b);
      fwrite(&pixel, sizeof(struct pixel), 1, out);
    }
    if (offset != 0) {
      for (size_t j = 0; j < offset; j++) {
        pixel.b = 0;
        pixel.r = 0;
        pixel.g = 0;
        fwrite(&pixel, sizeof(unsigned char), 1, out);
      }
    }
  }
  return WRITE_OK;

}
