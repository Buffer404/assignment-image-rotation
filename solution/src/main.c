#include "../include/formats/bmp.h"
#include "../include/transform/rotate.h"
#include "../include/utils/util.h"
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char **argv) {
    if (argc != 3) {
        printf("Введите путь до входящего и исходящего файлов\n");
        exit(1);
    }

    FILE *inputFile = NULL;
    FILE *outputFile = NULL;
    struct image *inputImage = NULL;
    struct image *outputImage = NULL;

    switch (open_file(&inputFile, argv[1], "rb")) {
            case OPEN_FAILED:
                fprintf(stderr, "No such file or directory\n");
                return 1;
            case OPEN_OK:
                fprintf(stdin, "Input file opened\n");
                break;
        }


    switch (from_bmp(inputFile, &inputImage)) {
        case READ_OK:
            fprintf(stdin, "Image loaded successfully\n");
            break;
        case READ_INVALID_HEADER:
            fprintf(stderr, "File has an invalid header\n");
            return 1;
            break;
        case READ_FAILED:
            fprintf(stderr, "Read error appeared\n");
            return 1;
            break;
        case READ_INVALID_BITS:
            fprintf(stderr, "The image not in RGB\n");
            return 1;
            break;
    }

    close_file(inputFile);

    outputImage = rotate(inputImage);

    deinitialize(inputImage);

    switch (open_file(&outputFile, argv[2], "wb")) {
            case OPEN_FAILED:
                fprintf(stderr, "No such file or directory\n");
                return 1;
            case OPEN_OK:
                fprintf(stdin, "Input file opened\n");
                break;
        }


    switch (to_bmp(outputFile, outputImage)) {
            case WRITE_OK:
                fprintf(stdin, "Image saved successfully\n");
                break;
            case WRITE_INVALID_SOURCE:
                fprintf(stderr, "Image broke 2\n");
                return 1;
                break;
            case WRITE_HEADER_ERROR:
                fprintf(stderr, "Error appeared while writing new image's header\n");
                return 1;
                break;
            case WRITE_ERROR:
                fprintf(stderr, "Error appeared while writing new image\n");
                return 1;
                break;
        }

    close_file(outputFile);

    deinitialize(outputImage);

    return 0;
}
